﻿#include <iostream>

long int Fibonacci(int n)
{
	if (n == 0)
		return 0;
	if (n == 1)
		return 1;
	return Fibonacci(n - 1) + Fibonacci(n - 2);
}

int main()
{
	int a;
	std::cin >> a;

	std::cout << Fibonacci(a) << std::endl;
}

