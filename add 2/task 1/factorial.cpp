﻿#include <iostream>

long int Factorial(int n)
{
	if (n == 0)
		return 1;
	return n * Factorial(n - 1);
}

int main()
{
	int a;
	std::cin >> a;

	std::cout << Factorial(a) << std::endl;
}
