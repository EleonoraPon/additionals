#include"Vec.hpp"
#include<iostream>

namespace el
{
	Vector::Vector(int a)
	{
		m_a = a;
		m_mas = new int[2*m_a];
		m_s = 0;//������ 
	}

	Vector::~Vector()
	{
		delete[] m_mas;
	}

	void Vector::Create(int n)
	{
		int b;
		std::cin >> b;
		m_mas[n] = b;
		m_s++;
	}

	int Vector::GetS() { return m_s; }
	int Vector::GetE(int i) { return m_mas[i]; }

	void Vector::CByIndex(int s, int num)
	{
		for (int i = m_s - 1; i >= s ; i--)
		{
			m_mas[i + 1] = m_mas[i];
		}
		m_mas[s] = num;
		m_s++;
	}

	void Vector::Sort()
	{
		for (int j = 0; j < m_s; j++)
			for (int i = j+1; i < m_s; i++)
				if (m_mas[j] > m_mas[i])
				{
					int t = m_mas[j];
					m_mas[j] = m_mas[i];
					m_mas[i] = t;
				}
	}
}