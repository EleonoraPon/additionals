﻿#include <iostream>
#include "Vec.hpp"

int main()
{
	int a;
	std::cin >> a;
	el::Vector mas(a);
	
	for (int i = 0; i < a; i++)
	{
		mas.Create(i);
	}
	
	std::cout << mas.GetE(1) << std::endl;
	
	mas.CByIndex(2, 67);

	mas.Sort();

	for (int i = 0; i < mas.GetS(); i++)
	{
		std::cout << mas.GetE(i) << " ";
	}
}